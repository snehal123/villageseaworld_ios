package auto.common;

import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.Assert;
import com.google.common.base.Function;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.SwipeElementDirection;

public class AppiumWrapper extends InitAppium {

    /**
     * The find method is the most frequent used method. This method takes a fixed
     * parameter:'element By locator' and optional integer argument for timeout In cases where we
     * need to wait longer time for an element to load, we can pass the required timeout. ex.
     * find(userName, 10) where 10 seconds timeout is requested.
     * 
     * @param locator
     * @param args
     * @return webelement
     */
    public static WebElement find(final By locator, int... args) {

        int timeout = (args.length > 0 ? args[0] : 3);
        try {
            FluentWait<AppiumDriver> wait = new FluentWait<AppiumDriver>(driver)
                    .withTimeout(timeout, TimeUnit.SECONDS)
                    .pollingEvery(200, TimeUnit.MILLISECONDS)
                    .ignoring(Exception.class)
                    .ignoring(NoSuchElementException.class);

            WebElement webelement = wait.until(new Function<AppiumDriver, WebElement>() {
                public WebElement apply(AppiumDriver driver) {
                    return driver.findElement(locator);
                }
            });
            return webelement;
        } catch (Exception e) {
            throw new NoSuchElementException();
            // return null;
        }
    }

    /**
     * Method to perform the Tap action on an element. Takes the By locator as parameter
     * 
     * @param locator
     */
    public void tapElement(By locator) {
        find(locator).click();
    }

    /**
     * This method performs swipe actions on a particular element when invoked with the Element and
     * direction to swipe. Typically used to swipe on date picker, list picker in iOS etc
     * 
     * @param locator
     * @param direction
     */
    public static void swipeElement(By locator, SwipeElementDirection direction) {
        MobileElement element = (MobileElement) find(locator);
        element.swipe(direction, 1000);
    }

    /**
     * Method to simulate swipe action on the screen in desired direction
     * 
     * @param direction
     */
    public static void swipeScreen(String direction) {

        int y = driver.manage().window().getSize().getHeight();
        int x = driver.manage().window().getSize().getWidth();
        try {
            switch (direction.toUpperCase()) {
            case "UP":
                driver.swipe(500, 10, 500, y - 10, 1000);
            case "DOWN":
                driver.swipe(500, y - 10, 500, 10, 1000);
            case "LEFT":
                driver.swipe(50, y / 2, x - 10, y / 2, 1000);
            case "RIGHT":
                driver.swipe(x - 50, y / 2, 10, y - 10, 1000);
            default:
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException ix) {
            System.out.println("Invalid directioN: Valid parameters- UP/DOWN/LEFT/RIGHT");
        }

    }

    /**
     * Type a value into an appropriate element by passing the value to be entered and the element
     * locator
     * 
     * @param inputValue
     * @param locator
     */
    public static void typeValue(String inputValue, By locator) {
        find(locator).sendKeys(inputValue);
    }

    /**
     * This method is specifically to use when needed to set PIN in an app. Using this sets pin very
     * quickly.
     * 
     * @param pinValue
     */
    public static void enterPin(String pinValue) {
        driver.getKeyboard().sendKeys(pinValue);
    }

    /**
     * Read the name/text attribute of the element
     * 
     * @param locator
     */
    public static void readValue(By locator) {
        find(locator).getText();
    }

    /**
     * Method to close the app
     */
    public static void closeApp() {
        driver.closeApp();
    }

    /**
     * Method to restart the App. If appium server is configured with "full reset" option then the
     * App is reinstalled. This will kill the current appium session and the test fails.
     */
    public static void restartApp() {
        driver.resetApp();
    }

    /**
     * Pass the locator of WebElement on which you want to perform the precise single tap action.
     * Typically used in situations like click on overlaid video play buttons etc
     * 
     * @param locator
     */
    public static void preciseTap(By locator) {
        WebElement element = find(locator);
        Point upperLeft = element.getLocation();

        final double finalXLocation = upperLeft.getX() + 5;
        final double finalYLocation = upperLeft.getY() + 5;
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("mobile: tap", new HashMap<String, Double>() {
            {
                put("tapCount", 1.0);
                put("touchCount", 1.0);
                put("duration", 1.0);
                put("x", finalXLocation);
                put("y", finalYLocation);
            }
        });
    }

    /**
     * This method takes a boolean parameter true/false. When user expects an Alert then pass "true"
     * as parameter else send "false" parameter
     * 
     * @param userExpectation
     * @throws NoAlertPresentException
     */
    public static void verifyPopupExists(boolean userExpectation)
            throws NoAlertPresentException {
        try {
            driver.switchTo().alert();
            Alert myAlert = driver.switchTo().alert();
            String alertTxt = myAlert.getText();
            if (userExpectation) {
                Assert.assertTrue(userExpectation,
                        "Popup dialog found with alert text :" + alertTxt);
            } else {
                Assert.assertTrue(!userExpectation,
                        "No Popup is expected, but found one with alert text :" + alertTxt);
            }

        } catch (NoAlertPresentException noAlert) {
            if (userExpectation) {
                Assert.assertTrue(!userExpectation, "Popup is expected, but not found one");
            } else {
                Assert.assertFalse(userExpectation, "No Popup found as expected");
            }
        }
    }

    /**
     * Method to accept a Popup. Call verifyPopupExists method before calling this method
     */
    public void acceptPopup() {
        Alert myAlert = driver.switchTo().alert();
        myAlert.accept();
    }

    /**
     * Method to read Popup Message. Call VerifyPopupExists method before calling this method
     * 
     * @return
     */
    public String getPopupText() {
        Alert myAlert = driver.switchTo().alert();
        return myAlert.getText();
    }

}
