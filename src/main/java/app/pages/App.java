package app.pages;

import auto.utils.AutoUtilities;
import auto.utils.FluentAssert;
import page.home.HomePage;

public class App {

    public HomePage homePage = null;
    public FluentAssert fluentAssert = null;
    public AutoUtilities utils = null;

    public App() {

        utils = new AutoUtilities();
        // Call this method with the test data json file name if needed for the project
        utils.loadTestData(System.getProperty("user.dir") + "/TestData/sampledata.json");

        // Initializing the fleuntAssert object for global use.
        fluentAssert = new FluentAssert();

        // Initializing the various pages of the app for use across all tests
        homePage = new HomePage();
    }

}
