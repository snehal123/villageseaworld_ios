package page.home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import auto.common.AppiumWrapper;

public class HomePage extends AppiumWrapper {

    public static By userName = By.id("userName");
    public static By loginButton = By.id("login");

    public void clickLogin() {

        tapElement(loginButton);

    }

}
